package model;

import java.util.ArrayList;
import java.util.List;

public class BraceClass  {
    String braceOpen = "(";
    String braceClose = ")";
    String plusSign = "+";
    String minusSign = "-";
    String multiSign = "*";
    String obelusSign = "/";
    int maxOpen;
    int maxClose;
    int open=0;
    int close=0;



    int exist;
    public List<String> stringArray;
    List<Integer> arrayBraceOpen = new ArrayList();
    List<Integer> arrayBraceClose = new ArrayList();
    List<String>  subStringNew;



    public void getArray(ArrayList array){
        this.stringArray =array;
    }
    public void clearArrayListBraceOpenAndClose(){
        arrayBraceOpen.clear();
        arrayBraceClose.clear();
    }
    public void scanBracePos() {
            int j =0;
            for (int k = 0; k < arrayBraceOpen.size(); k++) {

                if (arrayBraceClose.get(j) > arrayBraceOpen.get(k)) {
                        maxOpen = arrayBraceOpen.get(k);
                }
              }
            maxClose = arrayBraceClose.get(j);
            System.out.println(maxOpen);
            subStringNew = stringArray.subList(maxOpen,maxClose+1);
            if(subStringNew.size()==stringArray.size()){
                arrayStringSum();
            }else{
                subStringSum();
            }
            //arrayStringSum();
            System.out.println(stringArray);

        }


        public void subStringSum(){
            //setCheckListe();

            for(int b=0; b<subStringNew.size();b++) {
                if (subStringNew.get(b).equals(multiSign)) {
                    double beforeNumber = Double.parseDouble(subStringNew.get(b - 1));
                    double nextNumber = Double.parseDouble(subStringNew.get(b + 1));
                    double sumDouble = multTwoNumers(beforeNumber, nextNumber);
                    System.out.println("aus mal summe :"+sumDouble);
                    //replace and delete
                    subStringNew.set(b, Double.toString(sumDouble));
                    subStringNew.remove(b + 1);
                    subStringNew.remove(b - 1);

                }else if (subStringNew.get(b).equals(obelusSign)) {
                    double beforeNumber = Double.parseDouble(subStringNew.get(b - 1));
                    double nextNumber = Double.parseDouble(subStringNew.get(b + 1));
                    double sumDouble = obelusTwoNumers(beforeNumber, nextNumber);
                    System.out.println("aus geteilt summe :"+sumDouble);
                    //replace and delete
                    subStringNew.set(b, Double.toString(sumDouble));
                    subStringNew.remove(b + 1);
                    subStringNew.remove(b - 1);

                }else if(subStringNew.get(b).equals(braceOpen)||subStringNew.get(b).equals(braceClose)){

                    subStringNew.remove(b);
                }
            }
            for(int b2=0; b2<subStringNew.size();b2++) {
                if (subStringNew.get(b2).equals(minusSign)) {
                            double beforeNumber = Double.parseDouble(subStringNew.get(b2 - 1));
                            double nextNumber = Double.parseDouble(subStringNew.get(b2 + 1));
                            double sumDouble = minusTwoNumers(beforeNumber, nextNumber);
                            System.out.println("aus Minus summe :"+sumDouble);

                            //replace and delete
                            subStringNew.set(b2, Double.toString(sumDouble));
                            System.out.println(subStringNew);
                            subStringNew.remove(b2 + 1);
                            System.out.println(subStringNew);
                            subStringNew.remove(b2 - 1);
                            System.out.println(subStringNew);
                            System.out.println(stringArray+"minus Ende stringArray");

                } else if (subStringNew.get(b2).equals(plusSign)) {
                    double beforeNumber = Double.parseDouble(subStringNew.get(b2 - 1));
                    double nextNumber = Double.parseDouble(subStringNew.get(b2 + 1));
                    double sumDouble = addTwoNumers(beforeNumber, nextNumber);
                    System.out.println("aus plus summe :"+sumDouble);
                    //replace and delete
                    subStringNew.set(b2, Double.toString(sumDouble));
                    System.out.println(subStringNew);
                    subStringNew.remove(b2 + 1);
                    System.out.println(subStringNew);
                    subStringNew.remove(b2 - 1);
                    System.out.println(subStringNew);

                } else if (subStringNew.get(b2).equals(braceOpen) || subStringNew.get(b2).equals(braceClose)) {
                    System.out.println("brace remove start");
                    System.out.println(stringArray+"stringArray");
                    System.out.println(subStringNew+"subStringNew");
                    subStringNew.remove(b2);
                }
            }
            System.out.println("Ende subStringSum() ");
        }

        //For single brace to avoid subString error
        public void arrayStringSum(){
            multiPointBeforeLine();
            sumToEnd();
            for(int b=0; b<stringArray.size();b++) {
                 if(stringArray.get(b).equals(braceOpen)||stringArray.get(b).equals(braceClose)){
                    System.out.println(stringArray+"stringArray");
                    stringArray.remove(b);
                }
            }

        }

    public void multiPointBeforeLine() {

        for (int i = 0; i < stringArray.size(); i++) {
            String multiSign = "*";
            String obelusSign = "/";

            double sumDouble;
            if (stringArray.get(i).equals(multiSign)) {

                double beforeNumber = Double.parseDouble(stringArray.get(i - 1));
                double nextNumber = Double.parseDouble(stringArray.get(i + 1));
                sumDouble = multTwoNumers(beforeNumber, nextNumber);

                //replace and delete
                stringArray.set(i, Double.toString(sumDouble));
                stringArray.remove(i + 1);
                stringArray.remove(i - 1);

            } else if (stringArray.get(i).equals(obelusSign)) {

                double beforeNumber = Double.parseDouble(stringArray.get(i - 1));
                double nextNumber = Double.parseDouble(stringArray.get(i + 1));
                sumDouble = obelusTwoNumers(beforeNumber, nextNumber);

                //replace and delete
                stringArray.set(i, Double.toString(sumDouble));
                stringArray.remove(i + 1);
                stringArray.remove(i - 1);

            }

            System.out.println(stringArray);
        }

    }


    public void sumToEnd() {
        String plusSign = "+";
        String minusSign = "-";
        double sumDouble;
        for (int i = 0; i < stringArray.size(); i++) {
            if (stringArray.get(i).equals(plusSign)) {

                double beforeNumber = Double.parseDouble(stringArray.get(i - 1));
                double nextNumber = Double.parseDouble(stringArray.get(i + 1));
                sumDouble = addTwoNumers(beforeNumber, nextNumber);

                //replace and delete
                stringArray.set(i, Double.toString(sumDouble));
                stringArray.remove(i + 1);
                stringArray.remove(i - 1);

            } else if (stringArray.get(i).equals(minusSign)) {
                        double beforeNumber = Double.parseDouble(stringArray.get(i - 1));
                        double nextNumber = Double.parseDouble(stringArray.get(i + 1));
                        sumDouble = minusTwoNumers(beforeNumber, nextNumber);
                        System.out.println(sumDouble+"Normal - from stringarray");
                        //replace and delete
                        stringArray.set(i, Double.toString(sumDouble));
                        stringArray.remove(i + 1);
                        stringArray.remove(i - 1);
            }
        }
        System.out.println(stringArray);
    }
    public boolean checkPairBrace(){
        boolean back=false;
        if(arrayBraceOpen.size()==arrayBraceClose.size()){
            back= true;
        }
        return back;
    }
        public int scanArrayForBrace() {
             exist=0;
             open=0;
             close=0;
                 for (int i = 0; i < stringArray.size(); i++) {
                     if (stringArray.get(i).equals(braceOpen)) {
                         arrayBraceOpen.add(i);
                         exist++;
                         open++;
                         // stringArray.remove(i);
                     } else if (stringArray.get(i).equals(braceClose)) {
                         arrayBraceClose.add(i);
                         // stringArray.remove(i);
                         close++;
                     }
                 }
            System.out.println(arrayBraceClose);
            System.out.println(arrayBraceOpen);
            System.out.println("exist:"+exist);
            return exist;
        }

    public double addTwoNumers(double firstNumber,double secoundNumber){
        return firstNumber + secoundNumber;
    }
    public double minusTwoNumers(double firstNumber,double secoundNumber){
        return firstNumber - secoundNumber;
    }
    public double multTwoNumers(double firstNumber,double secoundNumber){
        return   firstNumber * secoundNumber;
    }
    public double obelusTwoNumers(double firstNumber,double secoundNumber){
        double x=0;
                if(firstNumber!=0&&secoundNumber!=0){
                      x= firstNumber / secoundNumber;
                }
        return x;
    }

    }

