module calculator {
    requires javafx.controls;
    requires javafx.graphics;



    opens controller to javafx.fxml;
    exports controller;
    exports model;
    opens model to javafx.fxml;
}