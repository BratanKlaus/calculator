package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.transform.Scale;
import model.Calculate;

public class Eventhandler implements EventHandler<ActionEvent> {
    private  Controller controller;

    public Eventhandler(Controller controller){
        this.controller = controller;

    }


    @Override
    public void handle(ActionEvent event){
        controller.buffertTextToArray();
    }
}
