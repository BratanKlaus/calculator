package controller;

import model.BraceClass;
import model.Calculate;
import model.TextIsCorrect;
import view.UserInterface;
import javafx.application.Application;
import javafx.stage.Stage;

public class Start extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        TextIsCorrect textIsCorrect = new TextIsCorrect();
        BraceClass braceClass = new BraceClass();
        UserInterface ui = new UserInterface();
        Calculate calculate = new Calculate(braceClass,textIsCorrect);

        Controller controller = new Controller(ui,calculate,textIsCorrect);


        ui.showView();
    }

}
