package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class EventhandlerDel implements EventHandler<ActionEvent> {
    private  Controller controller;

    public EventhandlerDel(Controller controller){
        this.controller = controller;
    }


    @Override
    public void handle(ActionEvent event){
        controller.delArray();
    }
}
