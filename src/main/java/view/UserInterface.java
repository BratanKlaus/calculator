package view;


import controller.Controller;
import controller.Eventhandler;
import controller.EventhandlerDel;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Calculate;

public class UserInterface extends Stage {


    GridPane gp = new GridPane();

    public String bufferToArray ="";

    public String getBufferToArray(){
        return this.bufferToArray = bufferToArray;

    }
    public void setOutPutField(String newNumber){
        this.outPutField.setText(newNumber);
        this.bufferToArray = newNumber;
    }



    //Output field
    TextField outPutField = new TextField();
    //calc buttons 0-9
    public Button zero = new Button("0");
    public Button one = new Button("1");
    public Button two = new Button("2");
    public Button three = new Button("3");
    public Button four = new Button("4");
    public Button five = new Button("5");
    public Button six = new Button("6");
    public Button seven = new Button("7");
    public Button eight = new Button("8");
    public Button nine = new Button("9");

    //Operation-Button
    public Button obelus = new Button("/");
    public Button multiplication = new Button("*");
    public Button minus = new Button("-");
    public Button plus = new Button("+");
    public Button sumSign = new Button("=");
    public Button dot = new Button(" .");
    public Button c = new Button(" c");
    public Button clear = new Button(" clear");

    public Button braceOpen = new Button(" (");
    public Button braceClose = new Button(" )");

    public void showView(){
        setTitle("My calculator");
        Scene szene;


        //1row
        gp.add(outPutField,4,0);
        //2row
        gp.add(obelus,0,1);
        gp.add(multiplication,1,1);
        gp.add(minus,2,1);
        gp.add(plus,3,1);
        //3row
        gp.add(seven,0,2);
        gp.add(eight,1,2);
        gp.add(nine,2,2);
        gp.add(braceOpen,3,2);
        gp.add(braceClose,4,2);
        //4row
        gp.add(four,0,3);
        gp.add(five,1,3);
        gp.add(six,2,3);
        gp.add(clear,3,3);
        //5row
        gp.add(one,0,4);
        gp.add(two,1,4);
        gp.add(three,2,4);
        gp.add(c,3,4);

        // 6-row
        gp.add(zero,0,5);
        gp.add(dot,2,5);
        gp.add(sumSign,3,5);

        //Action
        zero.setOnAction(e->{
            bufferToArray +="0";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+0);
        });
        one.setOnAction(e->{
            bufferToArray +="1";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+1);
        });
        two.setOnAction(e->{
            bufferToArray +="2";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+2);
        });
        three.setOnAction(e->{
            bufferToArray +="3";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+3);
        });
        four.setOnAction(e->{
            bufferToArray +="4";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+4);
        });
        five.setOnAction(e->{
            bufferToArray +="5";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+5);
        });
        six.setOnAction(e->{
            bufferToArray +="6";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+6);
        });
        seven.setOnAction(e->{
            bufferToArray +="7";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+7);
        });
        eight.setOnAction(e->{
            bufferToArray +="8";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+8);
        });
        nine.setOnAction(e->{
            bufferToArray +="9";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+9);
        });
        plus.setOnAction(e->{
            bufferToArray +="o"+"+"+"o";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+"+");
        });
        minus.setOnAction(e->{

            bufferToArray +="o"+"-"+"o";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+"-");
        });
        obelus.setOnAction(e->{
            bufferToArray +="o"+"/"+"o";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+"/");
        });
        multiplication.setOnAction(e->{
            bufferToArray +="o"+"*"+"o";
            System.out.println(bufferToArray);
            outPutField.setText(outPutField.getText()+"*");
        });
        braceOpen.setOnAction(e->{
            bufferToArray +="("+"o";
            outPutField.setText(outPutField.getText()+"(");
        });
        braceClose.setOnAction(e->{
            bufferToArray +="o"+")";
            outPutField.setText(outPutField.getText()+")");
        });
        dot.setOnAction(e->{
            bufferToArray +=".";
            outPutField.setText(outPutField.getText()+".");
        });
        clear.setOnAction(e->{
            bufferToArray ="";
            outPutField.setText("");
        });


        szene = new Scene(gp,300,300);
        setScene(szene);
        this.show();
    }
    public void addEventHandler(Eventhandler controller){
        System.out.println("text1");
        sumSign.setOnAction(controller);
        System.out.println("text2");
    }
    public void addEventHandlerDelete(EventhandlerDel controller){
        System.out.println("text4");
        c.setOnAction(controller);
        System.out.println("text5");
    }



}
